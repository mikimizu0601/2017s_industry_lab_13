package ictgradschool.industry.lab13.ex03;

import ictgradschool.Keyboard;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.concurrent.ThreadLocalRandom;

/**
 * TODO Create a multi-threaded program to calculate PI using the Monte Carlo method.
 */
public class ExerciseThreeMultiThreaded implements Runnable {
    /**
     * Estimates PI using a multi-threaded Monte Carlo method.
     */
    static long numSamples;
    private double estimatedPi = 0;


    protected double estimatePI(long numSamples) {
        ThreadLocalRandom tlr = ThreadLocalRandom.current();

        long numInsideCircle = 0;
        numSamples = numSamples/2;
        for (long i = 0; i < numSamples; i++) {

            double x = tlr.nextDouble();
            double y = tlr.nextDouble();

            if (Math.pow(x, 2.0) + Math.pow(y, 2.0) < 1.0) {
                numInsideCircle++;
            }

        }

        double estimate = 4.0 * (double) numInsideCircle / (double) numSamples;

        return estimate;
    }


    /** Program entry point. */
    public static void main(String[] args) {
        System.out.println("Enter the number of samples to use for approximation:");
        System.out.print("> ");

        numSamples = Long.parseLong(Keyboard.readInput());

        ExerciseThreeMultiThreaded e = new ExerciseThreeMultiThreaded();
        Thread thread = new Thread(e);


        ExerciseThre eMultiThreaded f = new ExerciseThreeMultiThreaded();
        Thread thread1 = new Thread(f);

        thread.start();
        thread1.start();
        System.out.println("Estimating PI...");
        try {
            thread.join();
            thread1.join();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }

        double estimatedPi = e.getEstimatedPi() + f.getEstimatedPi();


        long startTime = System.currentTimeMillis();
        long endTime = System.currentTimeMillis();
        long timeInMillis = endTime - startTime;

        double difference = Math.abs(estimatedPi/2 - Math.PI);
        double differencePercent = 100.0 * difference / Math.PI;
        NumberFormat format = new DecimalFormat("#.####");

        System.out.println("Estimate of PI: " + estimatedPi/2);
        System.out.println("Estimate is within " + format.format(differencePercent) + "% of Math.PI");
        System.out.println("Estimation took " + timeInMillis + " milliseconds");


    }

    private double getEstimatedPi(){
        return this.estimatedPi;
    }

    @Override
    public void run() {
        System.out.println("estimated pi is: " + estimatedPi+ Thread.currentThread());
        estimatedPi = estimatePI(numSamples);
        System.out.println("estimated pi is: " + estimatedPi+ Thread.currentThread());
        System.out.println(estimatedPi);
    }

}
